package ictgradschool.industry.lab_javaintro.ex01;

public class MyFirstProgram {
    public void start() {
        System.out.println("Hello World");
    }
    
    public static void main(String[] args) {
        MyFirstProgram p = new MyFirstProgram();
        p.start();
    }
}
